package com.tm;

import com.intellij.openapi.editor.Caret;
import com.intellij.openapi.editor.CaretAction;
import com.intellij.openapi.editor.Editor;

public class ReplacerSpace {
    protected String getReplaceString(String separatorValueFrom, String separatorValueTo, String targetValue) throws Exception {

            targetValue = firstUpperCase(targetValue);
            if (separatorValueFrom.isEmpty())
                separatorValueFrom = " ";

            if (targetValue.isEmpty())
                throw new Exception("Строка для замены отсутствует");

            for (int i = 0; i < separatorValueFrom.length(); i++) {
                if (targetValue.indexOf(separatorValueFrom.toCharArray()[i]) == -1 && !separatorValueFrom.contains(" ")) {
                    throw new Exception("В строке отсутствует символ для замены. [" + (separatorValueFrom.contains(" ") ? "Символ пробела" : separatorValueFrom) + "]");
                }
            }

            if (separatorValueTo.isEmpty())
                separatorValueTo = " ";

            return targetValue.trim().replace(separatorValueFrom, separatorValueTo);
    }

    private String firstUpperCase(String word){
        if(word == null || word.isEmpty()) return ""; //или return word;
        return word.substring(0, 1).toUpperCase() + word.substring(1);
    }
}

package com.tm;

import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.editor.*;
import com.intellij.openapi.editor.actionSystem.EditorActionHandler;
import com.intellij.openapi.editor.actionSystem.EditorWriteActionHandler;
import com.intellij.openapi.editor.actions.TextComponentEditorAction;
import com.intellij.openapi.util.TextRange;
import org.jetbrains.annotations.Nullable;

public class ReplacerTM extends TextComponentEditorAction {

    public ReplacerTM(EditorActionHandler defaultHandler) {
        super(defaultHandler);
    }

    public ReplacerTM() {
        this(new Handler());
    }

    private static class Handler extends EditorWriteActionHandler {
        private Handler() {
        }

        @Override
        public void executeWriteAction(final Editor editor, @Nullable Caret caret, DataContext dataContext) {
            Document document = editor.getDocument();
            ReplacerSpace replacerSpace = new ReplacerSpace();
            if (!document.isWritable()) {
                return;
            }

            runForCaret(editor, caret, c -> {
                StringBuffer stringBuffer = new StringBuffer();
                VisualPosition caretPosition = c.getVisualPosition();
                int selectionStartOffset = c.getSelectionStart();
                int selectionEndOffset = c.getSelectionEnd();

                String originalText = editor.getDocument().getText(new TextRange(selectionStartOffset, selectionEndOffset));
                String[] splitResult = originalText.split("->", 3);
                String replaceString;
                try {
                    if (splitResult.length <= 1) {
                        replaceString = replacerSpace.getReplaceString(" ", "_", splitResult[0].trim());
                        stringBuffer.append(replaceString);
                    } else if (splitResult.length <= 2) {
                        replaceString = replacerSpace.getReplaceString(" ", splitResult[0], splitResult[1].trim());
                        stringBuffer.append(replaceString);
                    } else {
                        replaceString = replacerSpace.getReplaceString(splitResult[0], splitResult[1], splitResult[2].trim());
                        stringBuffer.append(replaceString);
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
                editor.getDocument().replaceString(selectionStartOffset, selectionEndOffset, stringBuffer);
                c.moveToVisualPosition(caretPosition);
            });
        }

        private static void runForCaret(Editor editor, Caret caret, CaretAction action) {
            if (caret == null) {
                editor.getCaretModel().runForEachCaret(action);
            } else {
                action.perform(caret);
            }
        }
    }
}

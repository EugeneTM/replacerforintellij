package com.tm.test;

import com.jetbrains.rd.util.reactive.KeyValuePair;
import com.sun.javaws.exceptions.InvalidArgumentException;
import com.tm.ReplacerSpace;
import junit.framework.AssertionFailedError;
import org.hamcrest.core.Is;
import org.jetbrains.jps.api.CmdlineRemoteProto;
import org.jsoup.select.Evaluator;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class ReplacerSpaceTest extends ReplacerSpace {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    void getReplaceString_Space_To_Lower_Underline() throws Exception {
        String expected = "Test_string";
        String from = " ";
        String to = "_";
        String target = "Test string";

        String actual = getReplaceString(from, to, target);
        Assert.assertEquals(actual, expected);
    }

    @Test
    void getReplaceString_Space_Upper_Case() throws Exception {
        String expected = "Test_string";
        String from = " ";
        String to = "_";
        String target = "test string";

        String actual = getReplaceString(from, to, target);
        Assert.assertEquals(actual, expected);
    }

    @Test
    void getReplaceString_Target_Is_Empty(){
        String expected = "Строка для замены отсутствует";
        String from = " ";
        String to = "_";
        String target = "";
        Throwable trThrowable = assertThrows(Exception.class, () -> getReplaceString(from, to, target));
        assertEquals(trThrowable.getMessage(), expected);
    }

    @Test
    void getReplaceString_From_Is_Empty(){
        String expected = "В строке отсутствует символ для замены. [\\]";
        String from = "\\";
        String to = "_";
        String target = "test--string";
        Throwable trThrowable = assertThrows(Exception.class, () -> getReplaceString(from, to, target));
        assertEquals(trThrowable.getMessage(), expected);
    }
}